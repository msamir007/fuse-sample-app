export class Constants {
  // public static baseApiUrl = 'https://securingangularappscourse-api.azurewebsites.net/api/';
  public static baseUrl = 'http://localhost:50382';
  public static baseApiUrl = Constants.baseUrl + `/api`;
  public static baseOdataUrl = Constants.baseUrl + `/odata`;
  public static stsAuthority = 'http://localhost:5000';
  public static clientRoot = 'http://localhost:4200';

  public static actionView = 'view';
  public static actionEdit = 'edit';
  public static actionNew = 'new';
}
