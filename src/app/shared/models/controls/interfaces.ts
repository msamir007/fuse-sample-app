export interface FormControlError {
    errorName: string;
    errorMessage: string;
}

export interface GridPaginatedSortedFiltered {
    startRow: number;
    endRow: number;
    sortModel: GridSortModel[];
    filterModel: any;
}

interface GridSortModel {
    colId: string;
    sort: 'asc' | 'desc';
}

export class AuthPermissions {
    canView: string;
    canEdit: string;
    canDelete: string;
    canCreate: string;

    constructor(arg?: AuthPermissions) {
        let initialValue: AuthPermissions = {
            canView: '',
            canEdit: '',
            canDelete: '',
            canCreate: ''
        };
        initialValue = { ...initialValue, ...arg };
        for (const property in initialValue) {
            if (initialValue.hasOwnProperty(property)) { (this as any)[property] = (initialValue as any)[property]; }
        }
    }
}
