import { Observable } from 'rxjs';
import { AbstractControl } from '@angular/forms';

export class MaterialSelectOptions {
    constructor(arg: MaterialSelectOptions = {}) {
        if (arg) {
            for (const property in arg) {
                if (arg.hasOwnProperty(property)) { (this as any)[property] = (arg as any)[property]; }
            }
        }
    }

    data?: Observable<any[]> | any[];
    label?: string;
    hint?: string;
    display?: string;
    value?: string;
    errorMessages?: (formCtrl: AbstractControl) => string;
    disabled ? = false;
}
