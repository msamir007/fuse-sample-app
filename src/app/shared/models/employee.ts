export class Employee {

    id: number;

    name: string;

    phone: string;

    country: string;

    city: string;

    street: string;

    position: string;

    level: string;

    path: string;

}
