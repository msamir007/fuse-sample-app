import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AgGridModule } from 'ag-grid-angular';
import { SharedDirectivesModule } from 'app/shared/directives/shared-directives.module';
import { MaterialControlsModule } from '../material-controls/material-controls.module';
import { SearchFormContainerDirective } from './search-form-container.directive';
import { SearchListExpansionPanelComponent } from './search-list-expansion-panel.component';
import { SearchResultContainerDirective } from './search-result-container.directive';

@NgModule({
  imports: [
    CommonModule,
    MaterialControlsModule,
    AgGridModule.withComponents([]),
    SharedDirectivesModule
  ],
  exports: [SearchListExpansionPanelComponent, SearchResultContainerDirective, SearchFormContainerDirective],
  declarations: [SearchListExpansionPanelComponent, SearchResultContainerDirective, SearchFormContainerDirective]
})
export class SearchListExpansionPanelModule { }
