import { Component, OnInit, EventEmitter } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { AuthPermissions } from 'app/shared/models/controls/interfaces';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-grid-action-buttons',
  templateUrl: './grid-action-buttons.component.html',
  styleUrls: ['./grid-action-buttons.component.scss']
})
export class GridActionButtonsComponent implements OnInit, AgRendererComponent {
  
  crudPermissions: AuthPermissions = new AuthPermissions();

  private params: any;
  buttonClick: EventEmitter<any> = new EventEmitter();

  refresh(params: any): boolean {
    return true;
  }
  agInit(params: any): void {
    this.params = params;
  }

  onDelete(): void {
    this.params.delete(this.params);
  }
  onView(): void {
    this.params.view(this.params);
  }
  onEdit(): void {
    this.params.edit(this.params);
  }
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.crudPermissions = this.activatedRoute.snapshot.data['crudPermissions'] || this.crudPermissions;
  }

}

