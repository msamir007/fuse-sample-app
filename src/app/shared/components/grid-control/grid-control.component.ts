import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material';
import { AgGridAngular } from 'ag-grid-angular';
import { IDatasource, IGetRowsParams } from 'ag-grid-community';
import { ODataPagedResult } from 'angular-odata-es5';
import { Constants } from 'app/shared/constants/constants';
import { GridPaginatedSortedFiltered } from 'app/shared/models/controls/interfaces';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { GridHeaderOptions } from '../../models/controls/grid-control.model';
import { MaterialDialogService } from '../material-controls/material-dialog.service';
import { MaterialDialogComponent } from '../material-controls/material-dialog/material-dialog.component';
import { GridActionButtonsComponent } from './grid-action-buttons/grid-action-buttons.component';

@Component({
  selector: 'app-grid-control',
  templateUrl: './grid-control.component.html',
  styleUrls: ['./grid-control.component.scss'],
  providers: [MaterialDialogService],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class GridControlComponent implements OnInit {
  private _searchFilter;
  public _rowCount;

  @Input() showToolBar = false;
  @Input() rowData: any[];
  @Input() rowDataService: (obj: GridPaginatedSortedFiltered) => Observable<ODataPagedResult<any>>;
  @Input() columnOptions: any[];
  @Input() gridHeaderOptions: GridHeaderOptions;
  @Input() searchInput: any[];

  @Output() deleteRow: EventEmitter<{ rowIndex: number, data: any }> = new EventEmitter();
  @Output() viewRow: EventEmitter<{ rowIndex: number, data: any }> = new EventEmitter();
  @Output() editRow: EventEmitter<{ rowIndex: number, data: any }> = new EventEmitter();
  @Output() newRow: EventEmitter<any> = new EventEmitter();

  @ViewChild('agGrid', { static: true }) agGrid: AgGridAngular;
  @ViewChild('paginator', { static: true }) paginator: MatPaginator;

  dataSource: IDatasource;

  constructor(private dialogService: MaterialDialogService) {
  }

  ngOnInit(): void {
    // Append Action Column
    if (this.gridHeaderOptions.actionColumn) {
      {
        this.columnOptions = [
          ...this.columnOptions,
          {
            headerName: '',
            filter: false,
            sortable: false,
            minWidth: 140,
            cellRendererFramework: GridActionButtonsComponent,
            cellRendererParams: {
              delete: this.delete.bind(this),
              view: this.view.bind(this),
              edit: this.edit.bind(this)
            }
          }];
      }
    }
  }

  onGridReady(params): void {
    this.agGrid.api = params.api;
    this.agGrid.columnApi = params.columnApi;
    this.agGrid.api.sizeColumnsToFit();
    window.addEventListener('resize', () => {
      setTimeout(() => {
        params.api.sizeColumnsToFit();
      });
    });
  }

  beginSearch(obj: any): void {
    // Store search parameter
    this._searchFilter = obj;
    // Reset Paginator Index
    this.paginator.pageIndex = 0;
    // Set datasource.
    this.dataSource = {
      rowCount: this._rowCount,
      getRows: (p: IGetRowsParams) => {
        this.agGrid.api.showLoadingOverlay();
        // Invoke service from Input.
        this.rowDataService({ ...p, filterModel: this._searchFilter })
          .pipe(take(1))
          .subscribe(result => {
            this._rowCount = result.count;
            // TODO: Apply Flatten result here.
            p.successCallback(result.data, result.count);
            this.agGrid.api.hideOverlay();
          });
      }
    };
    this.agGrid.api.setDatasource(this.dataSource);
  }

  // ##### Refresh
  refreshData(): void {
    this.agGrid.api.refreshInfiniteCache();
  }
  // ##### Pagination
  onPageEvent(e: PageEvent): void {
    const api = this.agGrid.api;
    api.paginationGoToPage(e.pageIndex);
    api.paginationSetPageSize(e.pageSize);
    // paginationGetCurrentPage()
    // paginationGetTotalPages()
    // paginationGetRowCount()
    // paginationGoToNextPage()
    // paginationGoToPreviousPage()
    // paginationGoToFirstPage()
    // paginationGoToLastPage()
  }

  // ##### Dialog
  view(param): void {
    if (this.gridHeaderOptions.viewDialogClassType) {
      const dialogRef = this.dialogService.openDialog(this.gridHeaderOptions.viewDialogClassType, { data: param.data, actionType: Constants.actionView });
      dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
        if (result) {
          this.refreshData();
          this.viewRow.emit({ rowIndex: param.rowIndex, data: param.data });
        }
      });
    } else {
      this.viewRow.emit({ rowIndex: param.rowIndex, data: param.data });
    }
  }

  edit(param): void {
    if (this.gridHeaderOptions.editDialogClassType) {
      const dialogRef = this.dialogService.openDialog(this.gridHeaderOptions.editDialogClassType, { data: param.data, actionType: Constants.actionEdit });
      dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
        if (result) {
          this.refreshData();
          this.editRow.emit({ rowIndex: param.rowIndex, data: param.data });
        }
      });
    } else {
      this.editRow.emit({ rowIndex: param.rowIndex, data: param.data });
    }
  }

  delete(param): void {
    const dialogRef = this.dialogService.openDialog(MaterialDialogComponent, {}, { minHeight: 200, minWidth: 200 });
    dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
      if (result) {
        this.deleteRow.emit({ rowIndex: param.rowIndex, data: param.data });
      }
    });
  }

  create(): void {
    if (this.gridHeaderOptions.newDialogClassType) {
      const dialogRef = this.dialogService.openDialog(this.gridHeaderOptions.newDialogClassType, { actionType: Constants.actionNew });
      dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
        if (result) {
          this.refreshData();
          this.newRow.emit();
        }
      });
    } else {
      this.editRow.emit();
    }
  }
}
