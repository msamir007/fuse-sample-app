import { Component, Input, OnInit, Optional, Self, ViewChild } from '@angular/core';
import { AbstractControl, ControlValueAccessor, NgControl } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { MaterialSelectOptions } from 'app/shared/models/controls/material-select.model';
import { isObservable, Observable } from 'rxjs';

@Component({
  selector: 'app-material-select',
  templateUrl: './material-select.component.html',
  styleUrls: ['./material-select.component.scss']
})
export class MaterialSelectComponent implements OnInit, ControlValueAccessor {

  @Input() options: MaterialSelectOptions;

  @ViewChild('matSelect', { static: true }) matSelect: MatSelect;
  viewData: any[];
  selectedValue: any;
  isDisabled: boolean;

  abstractControl: AbstractControl;
  constructor(@Self() @Optional() public control: NgControl) {
    if (this.control != null) {
      this.control.valueAccessor = this;
    }
  }

  ngOnInit() {
    this.matSelect.errorStateMatcher = {
      isErrorState: () => {
        return !!(this.control && this.control.invalid && (this.control.dirty || this.control.touched));
      }
    };
    this.populateList(this.options.data);
    this.isDisabled = this.options.disabled;
  }

  onChanged(value) {
    this.propagateChange(value);
    this.matSelect.updateErrorState();
  }

  onTouched() {
    this.propagateTouched();
    this.matSelect.updateErrorState();
  }

  populateList(dataSource: Observable<any[]> | any[]) {
    if (isObservable(dataSource)) {
      dataSource.subscribe(d => {
        this.viewData = d;
      });
    } else {
      this.viewData = dataSource;
    }
    this.isDisabled = this.options.disabled;
  }

  get errorState() {
    return this.control.errors !== null && !!this.control.touched;
  }

  private propagateChange = (_: any) => { };
  private propagateTouched = () => { };

  writeValue(val: any): void {
    this.selectedValue = val;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.matSelect.setDisabledState(isDisabled); // disabled = isDisabled;
  }

}
