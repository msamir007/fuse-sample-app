import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {
    MatTableModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatToolbarModule,
    MatDialogModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatPaginatorIntl,

} from '@angular/material';
import { MaterialSelectComponent } from './material-select/material-select.component';
import { MaterialDialogComponent } from './material-dialog/material-dialog.component';
import { getArabicPaginatorIntl } from './mat-paginator-intl';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule,
        MatButtonModule,
        MatCardModule,
        MatListModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatToolbarModule,
        MatDialogModule,
        MatMomentDateModule,
        MatExpansionModule,
        MatPaginatorModule
    ],
    declarations: [MaterialSelectComponent, MaterialDialogComponent],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule,
        MatButtonModule,
        MatCardModule,
        MatListModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatToolbarModule,
        MatDialogModule,
        MatMomentDateModule,
        MatExpansionModule,
        MatPaginatorModule,

        MaterialSelectComponent,
        MaterialDialogComponent
    ],
    providers: [
        { provide: MatPaginatorIntl, useValue: getArabicPaginatorIntl() }
      ]
})

export class MaterialControlsModule {
}
