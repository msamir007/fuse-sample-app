import { Component, OnInit, forwardRef, ViewChild, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { ImageCropperComponent, ImageCroppedEvent } from 'ngx-image-cropper';
import { Constants } from '../../constants/constants';
@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => FileUploadComponent),
    multi: true
  }]
})
export class FileUploadComponent implements OnInit, ControlValueAccessor {
  imageUrl: string;

  private path: string;
  @Input() set imagePath(value: string) {
    if (value) {
      this.path = Constants.baseUrl + `/${value}`;
    }
  }
  get imagePath(): string {
    return this.path;
  }
  @ViewChild(ImageCropperComponent, { static: true }) imageCropper: ImageCropperComponent;
  imageFileChanged: any;
  showCropper = false;


  base64textString = '';
  imgPreview: Blob;

  constructor() { }
  // files: any = [];

  // uploadFile(event) {
  //   for (const index of event) {
  //     const element = event[index];
  //     this.files.push(element.name);
  //   }
  // }

  // deleteAttachment(index) {
  //   this.files.splice(index, 1)
  // }

  ngOnInit(): void {
  }

  handleFileSelect(evt): void {
    const files = evt.target.files;
    const file = files[0];
    if (files && file) {
      this.imageFileChanged = file;
      this.imgPreview = file;
      this._handleReaderLoaded(this.imgPreview);
    }
  }

  _handleReaderLoaded(file: any): void {
    this.propagateChange(file);
    // const reader = new FileReader();
    // reader.readAsBinaryString(file);
    // reader.onload = (readerEvt) => {
    //   const binaryString = readerEvt.target['result'];
    //   this.base64textString = btoa(binaryString);
    //   this.propagateChange(this.base64textString);
    // };
  }

  imageCropped(event: ImageCroppedEvent): void {
    this.imgPreview = event.file;
    console.log(this.imgPreview);
    this._handleReaderLoaded(this.imgPreview);
  }
  imageLoaded(): void {
    console.log('Image loaded');
  }
  cropperReady(): void {
    console.log('Cropper ready');
  }
  loadImageFailed(): void {
    console.log('Load failed');
  }

  onStartCrop(): void {
    this.showCropper = true;
    this.imageCropper.imageFileChanged = this.imageFileChanged;
  }

  onFinishCrop(): void {
    this.showCropper = false;
  }
  // -------------
  private propagateChange = (_: any) => { };
  private propagateTouched = () => { };

  writeValue(val: string): void {
    this.imageUrl = val;
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
  }
}

