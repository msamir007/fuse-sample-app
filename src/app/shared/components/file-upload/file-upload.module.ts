import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadComponent } from './file-upload.component';
// import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { ImagePreviewDirective } from './image-preview.directive';
import { DragDropFileDirective } from './drag-drop-file.directive';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MatButtonModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    ImageCropperModule
    // FileUploadModule
  ],
  declarations: [FileUploadComponent, ImagePreviewDirective, DragDropFileDirective],
  exports: [FileUploadComponent]
})
export class ImageFileUploadModule { }
