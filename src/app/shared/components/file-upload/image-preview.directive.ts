import { Directive, ElementRef, Input, Renderer, OnChanges, SimpleChanges } from '@angular/core';

@Directive({ selector: 'img[imgPreview]' })

export class ImagePreviewDirective implements OnChanges {

    @Input() imgPreview: any;

    constructor(private el: ElementRef) { }

    ngOnChanges(changes: SimpleChanges) {

        const reader = new FileReader();
        const el = this.el;

        reader.onloadend = function (e) {
            el.nativeElement.src = reader.result;
        };

        if (this.imgPreview) {
            return reader.readAsDataURL(this.imgPreview);
        }

    }

}
