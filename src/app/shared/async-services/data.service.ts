import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GridPaginatedSortedFiltered } from '../models/controls/interfaces';
import { ODataPagedResult, ODataService, ODataServiceFactory } from 'angular-odata-es5';
import { ODataFilterBuilder, IFilterExpressionResult } from 'ts-odata-filter';

@Injectable()
export class DataService<T> {
  constructor(
    protected url: string, protected httpClient: HttpClient, protected odata?: ODataService<T>) {

  }
  public getById(id): Observable<T> {
    return this.httpClient.get<T>(`${ this.url }/${ id }`);
  }

  public getAll(): Observable<T[]> {
    return this.httpClient
      .get<T[]>(this.url);
  }

  public create(post: T): Observable<T> {
    return this.httpClient
      .post<T>(`${ this.url }`, JSON.stringify(post));
  }

  public update(post: T, id: any): any {
    return this.httpClient
      .put(`${ this.url }/${ id }`, JSON.stringify(post));
  }

  public delete(id: any): any {
    return this.httpClient
      .delete(`${ this.url }/${ id }`);
  }

  public getAllOdata(): Observable<T[]> {
    return this.httpClient
      .get<T[]>(this.url);
  }

  public getAllPaginatedSortedFiltered(arg?: GridPaginatedSortedFiltered): Observable<ODataPagedResult<T>> {
    const query = this.odata.Query();
    // TODO: Expand, Select here

    // Loop Filter here)
    const filterObj = arg.filterModel;
    if (filterObj) {
      const filter = ODataFilterBuilder.build<T>((builder, prop) => {
        const filterExpression: IFilterExpressionResult<T, number>[] = [];
        for (const key in filterObj) {
          if (arg.filterModel.hasOwnProperty(key)) {
            if (filterObj[key] !== null) {
              filterExpression.push(builder.contains(prop[key], filterObj[key].toString()));
            }
          }
        }
        return builder.or(
          ...filterExpression
        );
      }).getString();

      query.Filter(filter);
    }

    // Loop Sort here 
    const sort = arg.sortModel.map(obj => Object.keys(obj).map(val => obj[val]).join(' ')).join(',');
    query.OrderBy(sort);

    // Get Pagination
    query.Skip(arg.startRow)
      .Top(arg.endRow - arg.startRow);

    // Execute Query with count
    return query.ExecWithCount();
  }
}
