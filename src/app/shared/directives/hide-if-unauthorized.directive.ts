import { Directive, Input, ElementRef, OnInit } from '@angular/core';
import { AuthService } from 'app/core/auth/auth.service';

@Directive({
  selector: '[hideIfUnauthorized]'
})
export class HideIfUnauthorizedDirective implements OnInit {

  @Input('hideIfUnauthorized') permission: string;

  constructor(private el: ElementRef, private authService: AuthService) { }

  ngOnInit() {
    if (this.permission && !this.authService.hasPermission(this.permission)) {
      this.el.nativeElement.style.display = 'none';
    }
  }
}
