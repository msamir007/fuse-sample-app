import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HideIfUnauthorizedDirective, DisableIfUnauthorizedDirective } from './';
@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    HideIfUnauthorizedDirective,
    DisableIfUnauthorizedDirective
  ],
  declarations: [HideIfUnauthorizedDirective, DisableIfUnauthorizedDirective],
})
export class SharedDirectivesModule { }
