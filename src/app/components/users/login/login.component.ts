import { Component, OnInit } from '@angular/core';
import { Login } from '../../../core/models/auth/login';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: Login = new Login('', '');
  invalidLogin: boolean;
  constructor(private service: LoginService, private router: Router,
    private notificationservice: NotificationsService, private authService: AuthService) { }

  login() {
    // this.service.login(this.user).subscribe(response => {
    //   const token = (response as any).token;
    //   localStorage.setItem('jwt', token);
    //   this.invalidLogin = false;
    //   this.notificationservice.info('Success!!', 'Login Successed!!');
    //   this.router.navigateByUrl('/products');
    // });
    this.authService.login();
  }
  ngOnInit() {
  }

}
