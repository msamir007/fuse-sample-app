import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from '../../../core/models/auth/login';
import { Register } from 'app/core/models/auth/register';
import { Constants } from 'app/shared/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class SignUpService {
  constructor(private http: HttpClient) { }
  SignUp(user: Register): any {
    const formData = new FormData();
    formData.append('UserName', user.UserName);
    formData.append('Email', user.Email);
    formData.append('Password', user.Password);
    formData.append('ConfirmPassword', user.ConfirmPassword);

    return this.http.post(Constants.stsAuthority+'Account/Register', formData);
    }
}
