import { Component, OnInit } from '@angular/core';
import { SignUpService } from './sign-up.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ValidatorFunctions } from 'app/shared/validations/validator-functions';
import { Register } from 'app/core/models/auth/register';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private service: SignUpService, private router: Router, private FormBuilder: FormBuilder, private authService: AuthService) { }

  user: Register;
  invalidSignUp: boolean;
  userform: FormGroup;
  submitted = false;
  SignUp() {
    this.submitted = true;

    if (this.userform.invalid) {
      return;
    }
    this.user = this.userform.value as Register;
    this.service.SignUp(this.user).subscribe(response => {
      // const token = (response as any).token;
      // localStorage.setItem('jwt', token);
      // this.invalidSignUp = false;
      this.router.navigate(['products']);
    }, err => {
      this.invalidSignUp = true;
      console.log(err);
    });
  }
  ngOnInit() {
    this.userform = this.FormBuilder.group({
      UserName: ['', [Validators.required]],
      Email: ['', [Validators.email, Validators.required]],
      Password: ['', [Validators.maxLength(100), Validators.minLength(6), Validators.required]],
      ConfirmPassword: ['', [Validators.required]],
    }, {
        validator: ValidatorFunctions.validateEqual('Password', 'ConfirmPassword')
      });
  }

  getErrorMessage = (formCtrl: AbstractControl) => {
    return ValidatorFunctions.getErrorMessage(formCtrl);
  }

  get f() { return this.userform.controls; }
}
