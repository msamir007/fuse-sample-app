import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, UrlTree } from '@angular/router';
import { AuthService } from 'app/core/auth/auth.service';
import { Observable } from 'rxjs';


@Injectable()
export class EmployeeGuard implements CanActivate {
  constructor(protected router: Router,
              protected authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> | boolean | UrlTree {
    return route.data['crudPermissions'] === undefined ? true :
      this.authService.hasPermission(route.data['crudPermissions']['canView']) ? true :
        this.router.navigate(['/home']);
  }
}
