import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from 'app/shared/async-services/data.service';
import { Observable } from 'rxjs';
import { ODataServiceFactory } from 'angular-odata-es5';
import { Employee } from 'app/shared/models/employee';


@Injectable()
export class EmployeesService extends DataService<any>  {
    url = '/employees';

    // Todo: Remove this line + fix interceptors
    private headers = new HttpHeaders({ enctype: 'multipart/form-data' });

    constructor(private http: HttpClient, odataFactory: ODataServiceFactory) {
        super('/employees', http, odataFactory.CreateService<Employee>('employees'));
    }

    public create(post: FormData): Observable<FormData> {
        return this.http
            .post<FormData>(`${this.url}`, post, { headers: this.headers });
    }

    public update(post: FormData, id: any): Observable<FormData> {
        return this.http
            .put<FormData>(`${this.url}/${id}`, post, { headers: this.headers });
    }
}
