import { NgModule } from '@angular/core';
import { ImageFileUploadModule } from 'app/shared/components/file-upload/file-upload.module';
import { GridControlModule } from 'app/shared/components/grid-control/grid-control.module';
import { MaterialControlsModule } from 'app/shared/components/material-controls/material-controls.module';
import { SearchListExpansionPanelModule } from 'app/shared/components/search-list-expansion-panel/search-list-expansion-panel.module';
import { EmployeeCrudComponent } from './employee-crud/employee-crud.component';
import { EmployeeRoutingModule } from './employee.routing.module';
import { EmployeesListComponent } from './employees-list/employees.list.component';
import { EmployeeGuard } from './shared/employee.guard';
import { EmployeesService } from './shared/employees.service';

@NgModule({
  declarations: [
    EmployeesListComponent,
    EmployeeCrudComponent
  ],
  imports: [
    MaterialControlsModule,
    GridControlModule,
    EmployeeRoutingModule,
    ImageFileUploadModule,
    SearchListExpansionPanelModule
  ],
  providers: [
    EmployeesService,
    EmployeeGuard
  ],
  entryComponents: [
    EmployeeCrudComponent
  ]

})

export class EmployeeModule {
}
