import { Component, OnInit, Input, Optional, Inject } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Employee } from 'app/shared/models/employee';
import { MaterialSelectOptions } from 'app/shared/models/controls/material-select.model';
import { EmployeesService } from '../shared/employees.service';
import { ValidatorFunctions } from 'app/shared/validations/validator-functions';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from 'app/shared/constants/constants';

@Component({
  selector: 'app-employee-crud',
  templateUrl: './employee-crud.component.html',
  styleUrls: ['./employee-crud.component.scss']
})
export class EmployeeCrudComponent implements OnInit {

  @Input() selectedEmployee: Employee;
  positionSelectOptions: MaterialSelectOptions;
  levelSelectOptions: MaterialSelectOptions;
  employeeForm: FormGroup;
  imageForm: FormGroup;

  pageName: string;

  get id(): AbstractControl {
    return this.employeeForm.get('id');
  }

  get name(): AbstractControl {
    return this.employeeForm.get('name');
  }

  get phone(): AbstractControl {
    return this.employeeForm.get('phone');
  }

  get country(): AbstractControl {
    return this.employeeForm.get('country');
  }

  get city(): AbstractControl {
    return this.employeeForm.get('city');
  }

  get street(): AbstractControl {
    return this.employeeForm.get('street');
  }

  get position(): AbstractControl {
    return this.employeeForm.get('position');
  }

  get level(): AbstractControl {
    return this.employeeForm.get('level');
  }

  get path(): AbstractControl {
    return this.employeeForm.get('path');
  }

  get imageFile(): AbstractControl {
    return this.imageForm.get('imageFile');
  }

  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public dialog: any,
    @Optional() public dialogRef: MatDialogRef<EmployeeCrudComponent>,
    private formBuilder: FormBuilder,
    private service: EmployeesService,
    private activatedRoute: ActivatedRoute) {
    if (this.activatedRoute.snapshot.url.length > 0) {
      this.pageName = this.activatedRoute.snapshot.url[1].path;
    }

  }

  ngOnInit(): void {
    if (this.dialog.actionType === Constants.actionNew) {
      this.selectedEmployee = new Employee();
    }
    else {
      this.selectedEmployee = this.dialog.data || this.selectedEmployee;
    }

    this.positionSelectOptions = new MaterialSelectOptions({
      data: [
        { name: 'Developer', id: 1 },
        { name: 'Tester', id: 2 },
        {
          name: 'Manager',
          id: 3
        }
      ],
      display: 'name',
      value: 'id',
      label: 'ادخل الوظيفة',
      // hint: 'Position Information',
      errorMessages: this.getErrorMessage
    });

    this.levelSelectOptions = new MaterialSelectOptions({
      data: [
        {
          name: 'Junior',
          id: 1
        }, {
          name: 'Mid',
          id: 2
        }, {
          name: 'Senior',
          id: 3
        }],
      display: 'name',
      value: 'id',
      label: 'المستوي',
      // hint: 'Level Information',
      errorMessages: this.getErrorMessage
    });

    this.employeeForm = this.formBuilder.group({
      id: [this.selectedEmployee.id],
      name: [this.selectedEmployee.name, [Validators.minLength(3), Validators.maxLength(15), Validators.required]],
      phone: [this.selectedEmployee.phone, [Validators.required,]],
      country: [this.selectedEmployee.country, []],
      city: [this.selectedEmployee.city, []],
      street: [this.selectedEmployee.street, []],
      position: [1, []],
      level: [1, []],
      path: [this.selectedEmployee.path, []],
    },
      // { validators: ValidatorFunctions.validateEqual('level', 'position') }
    );

    this.imageForm = this.formBuilder.group({
      imageFile: [null]
    });
    this.isViewMode();
  }

  onSubmit(): void {
    const formData = new FormData();
    formData.append('imageFile', this.imageFile.value);
    // Add new 
    if (this.dialog.actionType === Constants.actionNew) {
      this.employeeForm.value.id = 0;
      formData.append('employee', JSON.stringify(this.employeeForm.value));
      this.service.create(formData).subscribe(result => {
        if (this.dialogRef) {
          this.dialogRef.close(true);
        }
      });
    }
    // Edit
    else if (this.dialog.actionType === Constants.actionEdit) {
      formData.append('employee', JSON.stringify(this.employeeForm.value));
      this.service.update(formData, this.id.value).subscribe(
        (result) => {
          if (this.dialogRef) {
            this.dialogRef.close(true);
          }
        });
    }
  }

  getErrorMessage = (formCtrl: AbstractControl) => {
    return ValidatorFunctions.getErrorMessage(formCtrl);
  }

  isViewMode(): boolean {
    if (this.dialog.actionType === Constants.actionView) {
      for (const control of Object.keys(this.employeeForm.controls)) {
        this.employeeForm.controls[control].disable();
      }
      return true;
    }
    return false;
  }

}
