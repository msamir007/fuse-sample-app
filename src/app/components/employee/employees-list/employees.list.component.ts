import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ODataPagedResult } from 'angular-odata-es5';
import { GridControlComponent } from 'app/shared/components/grid-control/grid-control.component';
import { GridColumnOptions, GridHeaderOptions } from 'app/shared/models/controls/grid-control.model';
import { GridPaginatedSortedFiltered } from 'app/shared/models/controls/interfaces';
import { Employee } from 'app/shared/models/employee';
import { Observable } from 'rxjs';
import { EmployeeCrudComponent } from '../employee-crud/employee-crud.component';
import { EmployeesService } from '../shared/employees.service';

@Component({
  templateUrl: './employees.list.component.html',
  styleUrls: ['./employees.list.component.scss']
})

export class EmployeesListComponent implements OnInit {
  searchForm: FormGroup;

  // static: false --> very important
  @ViewChild('gridControl', { static: false }) grid: GridControlComponent;

  columnOptions = [
    new GridColumnOptions({ headerName: 'أسم الموظف', field: 'name' }),
    new GridColumnOptions({ headerName: 'تليفون', field: 'phone' }),
    new GridColumnOptions({ headerName: 'البلد', field: 'country' }),
    new GridColumnOptions({ headerName: 'المدينة', field: 'city' }),
    new GridColumnOptions({ headerName: 'الشارع', field: 'street' }),
    new GridColumnOptions({ headerName: 'الوظيفة', field: 'position' }),
    new GridColumnOptions({ headerName: 'المستوي', field: 'level' }),
  ];

  gridHeaderOptions = new GridHeaderOptions({
    actionColumn: true,
    viewDialogClassType: EmployeeCrudComponent,
    editDialogClassType: EmployeeCrudComponent,
    newDialogClassType: EmployeeCrudComponent,
    suppressPaginationPanel: false
  });

  constructor(private employeeService: EmployeesService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      name: [],
      phone: [],
      country: [],
      city: [],
    });
  }

  getEmployeesPaginatedSortedFiltered = (arg: GridPaginatedSortedFiltered): Observable<ODataPagedResult<Employee>> => {
    return this.employeeService.getAllPaginatedSortedFiltered(arg);
  }

  onDeleteClicked(param): void {
    this.employeeService.delete(param.data.id)
      .subscribe(() => this.grid.refreshData());
  }

  onBeginSearch(): void {
    this.grid.beginSearch(this.searchForm.value);
  }

  onCreate(): void {
    this.grid.create();
  }
}
