import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeesListComponent } from './employees-list/employees.list.component';
import { EmployeeGuard } from './shared/employee.guard';

const routes: Routes = [
  {
    path: '',
    component: EmployeesListComponent,
    canActivate: [EmployeeGuard],
    data: {
      crudPermissions: {
        canView: 'canViewEmployee',
        canEdit: 'canEditEmployee',
        canCreate: 'canCreateEmployee',
        canDelete: 'canDeleteEmployee',
      }
    },
  }, {
    path: 'new',
    component: EmployeesListComponent
  }, {
    path: ':id/edit',
    component: EmployeesListComponent
  }, {
    path: ':id/view',
    component: EmployeesListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})

export class EmployeeRoutingModule {
}
