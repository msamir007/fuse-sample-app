import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalaryListComponent } from './salary-list/salary-list.component';
import { SalaryGuard } from './shared/salary.guard';

const routes: Routes = [
  {
    path: '',
    component: SalaryListComponent,
    // canActivate: [SalaryGuard],
    // data: {
    //   crudPermissions: {
    //     canView: 'canViewSalary',
    //     canEdit: 'canEditSalary',
    //     canCreate: 'canCreateSalary',
    //     canDelete: 'canDeleteSalary',
    //   }
    // }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})

export class SalaryRoutingModule {
}
