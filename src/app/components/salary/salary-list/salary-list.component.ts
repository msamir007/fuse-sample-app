import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ODataPagedResult } from 'angular-odata-es5';
import { GridControlComponent } from 'app/shared/components/grid-control/grid-control.component';
import { GridColumnOptions, GridHeaderOptions } from 'app/shared/models/controls/grid-control.model';
import { GridPaginatedSortedFiltered } from 'app/shared/models/controls/interfaces';
import { Salary } from 'app/shared/models/salary';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { SalaryEditComponent } from '../salary-edit/salary-edit.component';
import { SalaryNewComponent } from '../salary-new/salary-new.component';
import { SalaryViewComponent } from '../salary-view/salary-view.component';
import { SalaryService } from '../shared/salary.service';
import { MaterialSelectOptions } from 'app/shared/models/controls/material-select.model';
import { EmployeesService } from 'app/components/employee/shared/employees.service';

@Component({
  selector: 'app-salary-list',
  templateUrl: './salary-list.component.html',
  styleUrls: ['./salary-list.component.scss'],
  providers: [EmployeesService, ]
})

export class SalaryListComponent implements OnInit {
  searchForm: FormGroup;
  employeeIdSelectOptions: MaterialSelectOptions;
  // static: false --> very important
  @ViewChild('gridControl', { static: false }) grid: GridControlComponent;

  columnOptions = [
    new GridColumnOptions({ headerName: 'Id', field: 'id' }),
    new GridColumnOptions({ headerName: 'الموظف', field: 'employeeId' }),
    new GridColumnOptions({ headerName: 'القيمة', field: 'salaryAmount' }),
    new GridColumnOptions({ headerName: 'التاريخ', field: 'salaryCreationDate' })
  ];

  gridHeaderOptions = new GridHeaderOptions({
    viewDialogClassType: SalaryViewComponent,
    editDialogClassType: SalaryEditComponent,
    newDialogClassType: SalaryNewComponent,
  });

  constructor(public salaryService: SalaryService,
    private formBuilder: FormBuilder,
    private employeesService: EmployeesService) { }

  ngOnInit(): void {
    this.employeeIdSelectOptions = new MaterialSelectOptions({
      data: this.employeesService.getAll(), // TODO: get paginated + Infinite scrolling
      label: 'الموظف',
      display: 'name', // TODO: set as default
      value: 'id', // TODO: set as default
    });


    this.searchForm = this.formBuilder.group({
      id: [],
      employeeId: [],
      salaryAmount: [],
      salaryCreationDateFrom: [],
      salaryCreationDateTo: [],
      isActivated: [],
      notes: [],
    });
  }

  getSalariesPaginatedSortedFiltered = (arg: GridPaginatedSortedFiltered): Observable<ODataPagedResult<Salary>> => {
    return this.salaryService.getAllPaginatedSortedFiltered(arg);
  }

  onDeleteClicked(param): void {
    this.salaryService.delete(param.data.id)
      .pipe(take(1))
      .subscribe(() => this.grid.refreshData());
  }

  onBeginSearch(): void {
    this.grid.beginSearch(this.searchForm.value);
  }

  onCreate(): void {
    this.grid.create();
  }
  // getSalariesPaginatedSortedFiltered = (arg: GridPaginatedSortedFiltered): Observable<ODataPagedResult<Salary>> => {
  //   return this.employeeService.getAllPaginatedSortedFiltered(arg);
  // }
}
