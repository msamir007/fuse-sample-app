import { NgModule } from '@angular/core';
import { GridControlModule } from 'app/shared/components/grid-control/grid-control.module';
import { MaterialControlsModule } from 'app/shared/components/material-controls/material-controls.module';
import { SearchListExpansionPanelModule } from 'app/shared/components/search-list-expansion-panel/search-list-expansion-panel.module';
import { SalaryListComponent } from './salary-list/salary-list.component';
import { SalaryEditComponent } from './salary-edit/salary-edit.component';
import { SalaryNewComponent } from './salary-new/salary-new.component';
import { SalaryViewComponent } from './salary-view/salary-view.component';
import { SalaryRoutingModule } from './salary.routing.module';
import { SalaryService } from './shared/salary.service';
import { SalaryGuard } from './shared/salary.guard';

@NgModule({
  declarations: [
    SalaryListComponent,
    SalaryNewComponent,
    SalaryViewComponent,
    SalaryEditComponent,
  ],
  imports: [
    GridControlModule,
    MaterialControlsModule,
    SalaryRoutingModule,
    SearchListExpansionPanelModule
  ],
  providers: [
    SalaryService,
    SalaryGuard
  ],
  entryComponents: [
    SalaryNewComponent,
    SalaryViewComponent,
    SalaryEditComponent,   
  ]
})

export class SalaryModule {
}
