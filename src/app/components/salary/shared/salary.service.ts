import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Salary } from 'app/shared/models/salary';
import { DataService } from 'app/shared/async-services/data.service';
import { ODataServiceFactory } from 'angular-odata-es5';

@Injectable()

export class SalaryService extends DataService<Salary> {
    constructor(http: HttpClient, odataFactory: ODataServiceFactory) {
        super('/salaries', http, odataFactory.CreateService<Salary>('salaries'));
    }
}
