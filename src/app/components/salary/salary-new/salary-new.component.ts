import { Component, Input, OnInit, Optional } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { MaterialSelectOptions } from 'app/shared/models/controls/material-select.model';
import { Salary } from 'app/shared/models/salary';
import { ValidatorFunctions } from 'app/shared/validations/validator-functions';
import { EmployeesService } from '../../employee/shared/employees.service';
import { SalaryService } from '../shared/salary.service';

@Component({
  selector: 'app-salary-new',
  templateUrl: './salary-new.component.html',
  styleUrls: ['./salary-new.component.scss'],
  providers: [EmployeesService,]
})

export class SalaryNewComponent implements OnInit {

  @Input() selectedSalary: Salary;
  salaryForm: FormGroup;
  employeeIdSelectOptions: MaterialSelectOptions;

  get id() {
    return this.salaryForm.get('id');
  }

  get employeeId() {
    return this.salaryForm.get('employeeId');
  }

  get salaryAmount() {
    return this.salaryForm.get('salaryAmount');
  }
  // ...

  constructor(@Optional() public dialogRef: MatDialogRef<SalaryNewComponent>,
    private formBuilder: FormBuilder,
    private service: SalaryService,
    private employeesService: EmployeesService) { }

  ngOnInit() {

    this.employeeIdSelectOptions = new MaterialSelectOptions({
      data: this.employeesService.getAll(), // TODO: get paginated + Infinite scrolling
      label: 'الموظف',
      errorMessages: this.getErrorMessage,
      display: 'name', // TODO: set as default
      value: 'id', // TODO: set as default
    });

    this.selectedSalary = new Salary();

    this.salaryForm = this.formBuilder.group({
      id: [],
      employeeId: [[Validators.required]],
      salaryAmount: [this.selectedSalary.salaryAmount, [Validators.min(0)]],
      salaryCreationDate: [this.selectedSalary.salaryCreationDate],
      salaryCreationDate2: [this.selectedSalary.salaryCreationDate2],
      notes: [this.selectedSalary.notes, [Validators.maxLength(100)]],
      notes2: [this.selectedSalary.notes2, [Validators.maxLength(100)]] // example field
    }, {
        validators: [ValidatorFunctions.validateEqual('notes', 'notes2'),
        ValidatorFunctions.validateDateGreater('salaryCreationDate', new Date()),
          // more validations
        ]
      });

  }
  onSubmit() {
    this.service.create(this.salaryForm.value).subscribe(result => {
      if (this.dialogRef) {
        this.dialogRef.close(true);
      }
    });
  }
  getErrorMessage = (formCtrl: AbstractControl) => {
    return ValidatorFunctions.getErrorMessage(formCtrl);
  }
}
