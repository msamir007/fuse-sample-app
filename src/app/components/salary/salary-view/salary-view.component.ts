import { Component, Inject, Input, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EmployeesService } from 'app/components/employee/shared/employees.service';
import { MaterialSelectOptions } from 'app/shared/models/controls/material-select.model';
import { Salary } from 'app/shared/models/salary';
import { ValidatorFunctions } from 'app/shared/validations/validator-functions';
import { SalaryEditComponent } from '../salary-edit/salary-edit.component';
import { SalaryService } from '../shared/salary.service';

@Component({
  selector: 'app-salary-view',
  templateUrl: './salary-view.component.html',
  styleUrls: ['./salary-view.component.scss'],
  providers: [EmployeesService,]
})

export class SalaryViewComponent implements OnInit {

  @Input() selectedSalary: Salary;
  salaryForm: FormGroup;
  employeeIdSelectOptions: MaterialSelectOptions;

  get id() {
    return this.salaryForm.get('id');
  }

  get employeeId() {
    return this.salaryForm.get('employeeId');
  }

  get salaryAmount() {
    return this.salaryForm.get('salaryAmount');
  }
  // ...

  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public selectedSalaryDialog: any,
    @Optional() public dialogRef: MatDialogRef<SalaryEditComponent>,
    private formBuilder: FormBuilder,
    private service: SalaryService,
    private employeesService: EmployeesService) { }

  ngOnInit() {
    this.employeeIdSelectOptions = new MaterialSelectOptions({
      data: this.employeesService.getAll(), // TODO: get paginated + Infinite scrolling
      label: 'الموظف',
      display: 'name', // TODO: set as default
      value: 'id', // TODO: set as default
    });

    this.selectedSalary = this.selectedSalaryDialog.data || this.selectedSalary;

    this.salaryForm = this.formBuilder.group({
      id: [this.selectedSalary.id],
      employeeId: [this.selectedSalary.employeeId],
      salaryAmount: [this.selectedSalary.salaryAmount],
      salaryCreationDate: [this.selectedSalary.salaryCreationDate],
      salaryCreationDate2: [this.selectedSalary.salaryCreationDate2],
      notes: [this.selectedSalary.notes],
      notes2: [this.selectedSalary.notes2] // example
    }, {
        validators: [ValidatorFunctions.validateEqual('notes', 'notes2'),
        ValidatorFunctions.validateDateGreater('salaryCreationDate', new Date()),
          // more validations
        ]
      });

    this.disableControls();
  }

  disableControls() {
    for (const control of Object.keys(this.salaryForm.controls)) {
      this.salaryForm.controls[control].disable();
    }
  }
  onConfirm() {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }

}
