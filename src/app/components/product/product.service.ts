import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { DataService } from 'app/shared/async-services/data.service';
import { Product } from 'app/shared/models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends DataService<Product> {
  constructor(httpClient: HttpClient) {
    super('/Product', httpClient);
  }
}
