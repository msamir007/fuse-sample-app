import { Component, OnInit } from '@angular/core';
import { Product } from '../../shared/models/product';
import { ProductService } from './product.service';
import { NgForm } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products: Product[];
  constructor(private service: ProductService, private notificationservice: NotificationsService) { }

  getAll() {
    this.service.getAll().subscribe(
      a =>  this.products = a
      );
  }

  ngOnInit() {
   // this.getAll();
    this.notificationservice.info('Hi', 'Welcome Back');
  }

  saveEditable(item: Product) {
    this.service.update(item, item.id)
    .subscribe(a => this.notificationservice.info('Updated!!', 'click to undo'));
    }

  Delete(id: number) {
    this.service.delete(id).subscribe(response => {
      this.getAll();
      this.notificationservice.info('deleted!!', 'click to undo');
    });
  }

  Add(form: NgForm) {

    this.service.create(form.value).subscribe(
      a => {this.notificationservice.info('Added!!', 'click to undo');
            this.getAll(); });
  }

}
