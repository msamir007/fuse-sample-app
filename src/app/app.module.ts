import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { FakeDbService } from 'app/fake-db/fake-db.service';
import { AppComponent } from 'app/app.component';
import { AppStoreModule } from 'app/store/store.module';
import { LayoutModule } from 'app/layout/layout.module';
import { MaterialControlsModule } from './shared/components/material-controls/material-controls.module';
import { InlineEditorModule } from '@qontu/ngx-inline-editor';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/users/login/login.component';
import { SignUpComponent } from './components/users/sign-up/sign-up.component';
import { ProductComponent } from './components/product/product.component';
import { AuthGuard } from './core/guards/auth.guard';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ImageFileUploadModule } from './shared/components/file-upload/file-upload.module';
import { MyInterceptor } from './core/interceptors/my-interceptor';
import { HttpResponseHandler } from './shared/async-services/http-response-handler.service';
import { LayoutComponent } from './containers/layout/layout.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { ODataConfiguration, ODataServiceFactory } from 'angular-odata-es5';
import { AppODataConfig } from './shared/constants/odata-config';
import { AppRoutingModule } from './app.routing.module';
const appRoutes: Routes = [

];

@NgModule({
    declarations: [
        AppComponent,
        ProductComponent,
        LoginComponent,
        SignUpComponent,
        // FooterComponent,
        LayoutComponent,
        HeaderComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,

        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay: 0,
            passThruUnknownUrl: true
        }),
        // ------------------
        FormsModule,
        InlineEditorModule,
        AngularFontAwesomeModule,
        MaterialControlsModule,
        SimpleNotificationsModule.forRoot(),
        ImageFileUploadModule,
        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        // AppStoreModule
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MyInterceptor,
            multi: true
        },
        HttpResponseHandler,
        {
            provide: ODataConfiguration,
            useClass: AppODataConfig
        },
        ODataServiceFactory,
    ],
})
export class AppModule {
}
