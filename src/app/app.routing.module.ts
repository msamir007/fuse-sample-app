import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { LoginComponent } from './components/users/login/login.component';
import { SignUpComponent } from './components/users/sign-up/sign-up.component';
import { ProductComponent } from './components/product/product.component';
import { AuthGuard } from './core/guards/auth.guard';

/**
 * Main app routing. Includes the initial empty redirect.
 * @type {[{path: string; redirectTo: string; pathMatch: string}]}
 */
const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'signUp', component: SignUpComponent },
    { path: 'home', component: ProductComponent },
    { path: 'salary', loadChildren: './components/salary/salary.module#SalaryModule', canActivate: [AuthGuard] },
    { path: 'employee', loadChildren: './components/employee/employee.module#EmployeeModule', canActivate: [AuthGuard] },

    {
        path: '**',
        redirectTo: 'home'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {enableTracing: true})],
    exports: [RouterModule],
    providers: []
})

export class AppRoutingModule {
}
