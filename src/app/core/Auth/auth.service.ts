import { Injectable } from '@angular/core';
import { UserManager, User, WebStorageStateStore, Log, UserManagerSettings } from 'oidc-client';
import { Constants } from '../../shared/constants/constants';
import { HttpClient } from '@angular/common/http';
import { take, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    private _userManager: UserManager;
    private _user: User;

    public userClaims: any;

    constructor(private http: HttpClient) {
        Log.logger = console;
        const config: UserManagerSettings = {
            loadUserInfo: true,
            filterProtocolClaims: true,
            authority: Constants.stsAuthority, // url for identity server provider
            client_id: 'angular-client',
            redirect_uri: 'http://localhost:4200/assets/oidc-login-redirect.html', // redirect after success login
            scope: 'profile openid roles email employeeCrudPermissions AdminClientId_api',
            response_type: 'id_token token', // respons type for impilict flow (id_token token)
            post_logout_redirect_uri: `http://localhost:4200/postLogout=true`,
            userStore: new WebStorageStateStore({ store: window.localStorage }),
            automaticSilentRenew: true,
            silent_redirect_uri: `http://localhost:4200/assets/silent-redirect.html`
            // both redirect Uris need to be set.
        };
        this._userManager = new UserManager(config);
        this._userManager.getUser().then(user => {
            if (user && !user.expired) {
                this._user = user;
                this.getClaims();
                // console.log(user);
            }
        });
        this._userManager.events.addUserLoaded(() => {
            this._userManager.getUser().then(user => {
                this._user = user;
                this.getClaims();
            });
        });
    }

    login(): Promise<any> {
        return this._userManager.signinRedirect();
    }

    logout(): Promise<any> {
        return this._userManager.signoutRedirect();
    }

    isLoggedIn(): boolean {

        return this._user && this._user.access_token && !this._user.expired;
    }

    getAccessToken(): string {
        return this._user ? this._user.access_token : '';
    }

    signoutRedirectCallback(): Promise<any> {
        return this._userManager.signoutRedirectCallback();
    }

    getClaims(): void {
        const arrayToObject = (array: any[], keyField: string, valueField?: string) =>
            array.reduce((obj, item) => {
                obj[item[keyField]] = valueField ? item[valueField] : item;
                return obj;
            }, {});

        this.http.get<{ claims: any[], pageSize: number, totalCount: number }>(`http://localhost:5002/api/Users/${ this._user.profile.sub }/Claims?page=1&pageSize=100`)
            .pipe(
                take(1),
                map(res => arrayToObject(res.claims, 'claimType', 'claimValue'))
            )
            .subscribe(x => {
                this.userClaims = x;
                console.log(this.userClaims);
            });
    }

    hasPermission(claim: string): boolean {
        return this.isLoggedIn() && claim in this.userClaims;
    }
}
